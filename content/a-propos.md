---
#author: "Emmanuel Le Nohaïc"
title: "A propos"
#description: "Emmanuel Le Nohaïc's about webpage : another sysadmin description :/"
ShowReadingTime: false
ShowToc: false
weight: 2
params:
ShowShareButtons: true
---

Quelques informations sur moi : je suis un administrateur système et réseau, intéressé par les logiciels libres. J'apprécie également penser et mettre en place des systèmes reproductibles, hautement disponibles et tolérants aux pannes.

Ci-dessous quelques outils et logiciels que j'utilise au quotidien : 
- Debian, Ubuntu, Centos (pour le dernier, vraiment quand je n'ai pas le choix 😉)
- Bash / Python scripting
- Firewalling avec iptables / firewalld
- Automation avec Gitlab-ci, Drone et Jenkins
- Template de machines virtuelles avec Packer sur Proxmox et Vspheres
- Déploiements avec Terraform
- Administration de plusieurs noeuds Proxmox VE et sauvegardes avec Proxmox Backup

Cette liste n'est pas exhaustive et devrait être complétée, un jour.
Si vous voulez plus d'informations (ou si vous voulez simplement discuter 😄), n'hésitez pas à me contacter en utilisant les informations sur la page d'accueil.

## Footer

J'essaye la plupart du temps que j'accorde à ce site, de rédiger en français et en anglais. Cela me permet de maintenir un usage de la langue anglaise, et d'essayer d'améliorer mon niveau.
Les rédactions d'articles ayant lieu par défaut en français (et souvent sans relecture :slightly_smiling_face:), j'effectue la traduction assez rapidement, ce qui peut aboutir à de belles erreurs ou incompréhensions.
Si c'est le cas, n'hésitez pas à me remonter l'information si cela fait saigner vos yeux.

HTH. :grinning:
