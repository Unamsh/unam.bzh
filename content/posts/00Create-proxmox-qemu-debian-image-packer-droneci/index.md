---
title: "ProxySQL failover with Keepalived"
author: "Unam"
date: "2022-10-02"
description: "How to configure keepalived to build a highly available ProxySQL cluster"
tags: ["mysql", "proxysql", "systemd"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
draft: true
series: ["ProxySQL"]
ShowBreadCrumbs: true
---

# Introduction



Enjoy :wink:
