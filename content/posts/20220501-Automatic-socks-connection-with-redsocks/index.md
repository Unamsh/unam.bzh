---
title: "Etablir une connexion socks5 permanente et automatique sur Ubuntu"
author: "Unam"
date: "2022-05-01"
description: "How to enable and start automagically a socks5 connection for all system applications using redsocks, iptables and systemd"
description: "Comment configurer et démarrer automatiquement une connexion socks5 pour toutes les applications à l'aide de redsocks, iptables et systemd"
tags: ["iptables", "redsocks", "systemd"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
ShowBreadCrumbs: true
---

Il est parfois nécessaire d'avoir à s'authentifier sur un proxy socks pour plusieurs raisons comme accéder à un site web, établir une session ssh, etc.

Ici nous allons voir comment établir cette connexion de manière transparente pour le système, même si votre application que vous souhaitez utiliser ne supporte pas la connexion à un proxy socks.

L'identifiant et le mot de passe de connexion seront stockés dans le fichier de configuration de redsocks, l'un des objectifs étant de ne plus avoir à s'en souvenir 😉


# Comment ça fonctionne

  - Des règles iptables redirigent le traffic pour une destination spécifique vers un port local.
  - Redsocks écoute sur ce port, s'authentifie sur le proxy distant et envoie le trafic.
  - Systemd démarre et automatise le tout.


## Configuration de l'environnement

### Redsocks

Il est possible d'installer redsocks depuis les sources ou directement depuis le gestionnaire de paquets.

```bash
apt update
apt install redsocks
```

La configuration redsocks est située dans `/etc/redsocks.conf`. Elle est assez simple et facile à comprendre : 

```
base {
 log_debug = on;
 log_info = on;
 log = "stderr";
 daemon = off;
 redirector = iptables;
}

redsocks {
    // Local IP listen to
    local_ip = 127.0.0.1;
    // Port to listen to
    local_port = 12345;
    // Remote proxy address
    ip = MY.PROXY.ADDRESS;
    port = 443;
    // Proxy type
    type = socks5;
    // Username to authorize on proxy server
    login = MYLOGIN;
    // Password for a proxy user
    password = MYPASSWORD;
    // Do not disclose real IP
    disclose_src = false;
}
```

La configuration redsocks est terminée, on peut le démarrer avec systemd : 

```
systemctl enable --now redsocks
```

On vérifie que tout est au vert : 

```
systemctl status redsocks
```

Redsocks est maintenant configuré pour démarrer automatiquement. Pour l'instant le trafic n'est pas intercepté, la redirection vers le proxy distant ne fonctionne donc pas.

### Configuration d'iptables

J'ai écris un petit bout de script pour ajouter les règles iptables automagiquement. Le script est écrit dans `/usr/local/bin/iptablesSocks.sh` : 

```bash
#!/bin/bash

arg=${1}

add_rules () {
	iptables -t nat -F
	iptables -t nat -N REDSOCKS
	iptables -t nat -A REDSOCKS -d 172.17.0.0/24 -p tcp -j REDIRECT --to-ports 12345
	iptables -t nat -A REDSOCKS -d 172.17.1.0/24 -p tcp -j REDIRECT --to-ports 12345
	iptables -t nat -A OUTPUT -p tcp -j REDSOCKS
}

delete_rules () {
	iptables -t nat -F
	iptables -t nat -X
}

case ${arg} in
	start	) add_rules ;;
	stop	) delete_rules ;;
	*	) echo "no matching command" 
		exit 0
		;;
esac
```

  - Dans la fonction `add_rules`, on ajoute les règles pour envoyer le trafic pour tcp/172.17.{0,1}.0/24 vers tcp/localhost/12345
  - Dans la fonction `delete_rules`, on flush les règles.
  - On peut donc utiliser ce script avec les arguments `start` et `stop`.

Maintenant redsocks fonctionne, la configuration iptables aussi. Il reste à "interfacer" les deux pour être certain que les règles iptables sont chargées au lancement de redsocks, et effacées lors de son arrêt.

### Systemd

Le paquet redsocks fournit un fichier service systemd. Il est possible dans ces fichiers de surcharger tout ou partie de la configuration à l'aide de la commande  `systemctl edit`.

Exemple : 

```bash
sudo systemctl edit redsocks
```

> Note : Il n'est pas nécessaire d'ajouter .service à la fin, ou le chemin complet vers le fichier service.

Et tada : 

![050f06a4be6147f0d4dccb39e254feff.png](images/TcrkIS.png#center)

Commen indiqué en commentaire, il est possible d'ajouter du contenu entre les lignes **"### Anything between[...]"** and **"### Lines below this"**.

Ci-dessous la configuration utilisée : 

```
[Unit]
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
Type=simple
Restart=on-failure
RestartSec=5s
ExecStartPost=//usr/local/bin/iptablesSocks.sh start
ExecStopPost=/usr/local/bin/iptablesSocks.sh stop
```

Une fois le fichier sauvegardé, on peut en vérifier le contenu en lisant `/etc/systemd/system/redsocks.service.d/override.conf`

Pour tester la configuration, on recharge la configuration systemd :  

```bash
sudo systemctl daemon-reload
```

On relance redsocks : 

```bash
sudo systemctl restart redsocks
```

Les règles iptables sont visibles avec ;

```
sudo iptables -t nat -L
```

En sortie : 

```
Chain REDSOCKS (1 references)
target     prot opt source               destination         
REDIRECT   tcp  --  anywhere             172.17.0.0/24        redir ports 12345
REDIRECT   tcp  --  anywhere             172.17.1.0/24        redir ports 12345
```

Enjoy :wink:
