---
title: "Permanent socks5 connection"
author: "Unam"
date: "2022-05-01"
description: "How to enable and start automagically a socks5 connection for all system applications using redsocks, iptables and systemd"
tags: ["iptables", "redsocks", "systemd"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
ShowBreadCrumbs: true
---

We often have to authenticate against a socks proxy for differents purposes :
  - access to a website
  - establish a ssh connection
  -...

Here I'm going to explain how to do it permanently and in a transparent way for the system.

My need here is to avoid manual connection involving login and password I could'nt remember 😉 


# How it works

  - Iptables rules redirect trafic for a specific destination to a local port.
  - Redsocks listens on this port and trafic to the remote proxy.
  - Systemd makes it working fine.


## Setup env

### Redsocks

Install from sources or package manager (On Ubuntu / Debian)

```bash
apt update
apt install redsocks
```

The redsocks configuration file is located under `/etc/redsocks.conf`. For my needs it's very simple and easy to understand : 

```
base {
 log_debug = on;
 log_info = on;
 log = "stderr";
 daemon = off;
 redirector = iptables;
}

redsocks {
    // Local IP listen to
    local_ip = 127.0.0.1;
    // Port to listen to
    local_port = 12345;
    // Remote proxy address
    ip = MY.PROXY.ADDRESS;
    port = 443;
    // Proxy type
    type = socks5;
    // Username to authorize on proxy server
    login = MYLOGIN;
    // Password for a proxy user
    password = MYPASSWORD;
    // Do not disclose real IP
    disclose_src = false;
}
```

Ok, redoscks must work. You can try to start it using the systemd service unit provided : 

```
systemctl enable --now redsocks
```

Then check if everything is ok :

```
systemctl status redsocks
```

Now Redsocks is configured to automatically start at boot, fine. 

But we can't catch the trafic that we want to send to the remote socks proxy yet.


### Iptables configuration

I made a little script to automagically apply iptables rules in `/usr/local/bin/iptablesSocks.sh` : 

```bash
#!/bin/bash

arg=${1}

add_rules () {
	iptables -t nat -F
	iptables -t nat -N REDSOCKS
	iptables -t nat -A REDSOCKS -d 172.17.0.0/24 -p tcp -j REDIRECT --to-ports 12345
	iptables -t nat -A REDSOCKS -d 172.17.1.0/24 -p tcp -j REDIRECT --to-ports 12345
	iptables -t nat -A OUTPUT -p tcp -j REDSOCKS
}

delete_rules () {
	iptables -t nat -F
	iptables -t nat -X
}

case ${arg} in
	start	) add_rules ;;
	stop	) delete_rules ;;
	*	) echo "no matching command" 
		exit 0
		;;
esac
```

  - In the `add_rules` function, we add rules to send trafic for tcp/172.17.{0,1}.0/24 to tcp/localhost/12345
  - In the `delete_rules` function, we flush the rules.
  - Then we can use the script with `start` and `stop` args.

Ok, now Redsocks works fine, iptables too. We need to "connect" both of them to be sure that when redsocks starts, iptables rules are loaded and disabled when redsocks stop.

### Systemd

The redsocks package provides a systemd service file. It is possible to override part of service files with custom configurations using the `systemctl edit` command.

Example : 

```bash
sudo systemctl edit redsocks
```

> Note : It's not necessary to append .service or the full path to service file.

And tada : 

![050f06a4be6147f0d4dccb39e254feff.png](images/TcrkIS.png#center)

As indicated, we can add stuff between the lines **"### Anything between[...]"** and **"### Lines below this"**.

Below the configuration I wrote in this configuration file : 

```
[Unit]
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
Type=simple
Restart=on-failure
RestartSec=5s
ExecStartPost=//usr/local/bin/iptablesSocks.sh start
ExecStopPost=/usr/local/bin/iptablesSocks.sh stop
```

Once you have saved this file, you can verify it by parsing `/etc/systemd/system/redsocks.service.d/override.conf`

If you want to test the configuration, reload systemd : 

```bash
sudo systemctl daemon-reload
```

Restart redsocks : 

```bash
sudo systemctl restart redsocks
```

Iptables rules shoud appear using : 

```
sudo iptables -t nat -L
```

With output : 

```
Chain REDSOCKS (1 references)
target     prot opt source               destination         
REDIRECT   tcp  --  anywhere             172.17.0.0/24        redir ports 12345
REDIRECT   tcp  --  anywhere             172.17.1.0/24        redir ports 12345
```

Enjoy :wink:
