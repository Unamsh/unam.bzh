---
title: "Générer un diagramme d'appel depuis un fichier pcap"
author: "Unam"
date: "2021-12-01"
description: "Comment générer un diagramme d'appel depuis un fichier pcap à l'aide de python et mermaid-js"
tags: ["python", "sip", "scripting"]
categories: ["dev", "sysadmin"]
ShowToc: false
weight: 2
draft: false
ShowBreadCrumbs: true
---

La fonction principale de ce script est de générer un diagram d'appel, comme le fait Wireshark lorsque l'on capture des flux SIP/RTP, evidemment sans avoir besoin d'ouvrir Wireshark et d'interagir graphiquement avec.

![43eb28c56995898e2f3fa528bdfbfbb7.png](images/9d646e52d331482e9755350350d34d76.png#center)

Petit plus, il peut être utilisé pour générer automatiquement des diagrammes à partir de plusieurs fichiers pcap, ou directement dans une interface web.

Dans le but d'aller un petit peu plus loin en python, j'ai tenté d'écrire un script pour répondre à ce besoin. Ce n'est peut être pas un chef d'oeuvre, mais il a le mérite de fonctionner.

# Comment ça marche

Le script utilise différents librairies : 
- python3 : obviously
- tshark et pyshark : pour lire le contenu de la capture
- markdown et md-mermaid : pour écrire et générer les diagrammes

Pour finir, git si vous souhaitez cloner directement le projet [depuis github](https://github.com/fulljackz/DrawMyCall)

> A noter que le script a été testé et fonctionne bien sur Ubuntu 21.10 et sur une install fraîche de Debian 11.

## Environnement 

Installation des dépendances : 

```bash
apt update
apt install git tshark python3-pip --no-install-recommends
pip3 install pyshark markdown md-mermaid
```

Clone du repo

```bash
git clone https://github.com/fulljackz/DrawMyCall.git
cd DrawMyCall
```

On y est, voici le contenu : 

```
├── drawmycall.py
├── html
│   ├── mermaid.css
│   ├── mermaid.min.js
│   └── sip-rtp-g722.pcap.html
├── img
│   ├── mermaid.png
│   ├── mermaid-time.png
│   └── wireshark.png
├── pcap_samples
│   ├── sip-rtp-g722.pcap
│   ├── sip-rtp-g729a.pcap
│   ├── sip-rtp-gsm.pcap
│   ├── sip-rtp-ilbc.pcap
│   ├── sip-rtp-lpc.pcap
│   └── sip-rtp-opus.pcap
└── README.md
```

- drawmycall.py est le script qui nous intéresse.
- **./html/** est le répertoire qui contient les fichiers `.css` et `.js`, utiles pour générer le fichier `pcap.html`.
- **./img/** est un répertoire servant aux screenshosts de la documentation.
- **./pcap_samples/** contient des fichiers `.pcap` pour tester. Ces fichiers sont tous issues du wiki Wireshark.

Si vous ouvrez le fichier `sip-rtp-g722.pcap.html` présent dans le répertoire `./html/` vous aurez un aperçu de diagramme produit par le script : 
![c4ac7f1809fb62d767ce94f4f9c57c90.png](images/0d95b1ad9c504d1bac70640b7afd5d7d.png#center)

## On essaye

La première à lire est l'aide : 

```bash
./drawmycall.py --help
usage: drawmycall.py [-h] -f FILE [-t]

optional arguments:
  -h, --help            show this help message and exit
  -t, --time            Add time on diagram

required arguments:
  -f FILE, --file FILE  path to your pcap file
```

On peut générer un diagramme à partir du fichier `./pcap_samples/sip-rp-opus.pcap`.

Avec wireshark :

![73500313b5056e4e9dbdd5bd1dc345cb.png](images/001956d217a5422792a7f8aad9fec1c5.png)

Avec drawmycall.py :

```bash
./drawmycall.py -f ./pcap_samples/sip-rtp-opus.pcap
```

Ouvrir le fichier `./html/sip-rtp-opus.pcap.html`

![500490cfc96946519ebf22b01b7bf1d6.png](images/34ef33ccacf54825b26e0ccc85b265da.png#center)

> Note : Pour éviter d'afficher tous les échanges rtp, il n'y a qu'une seule ligne de tracée.

Si besoin d'avoir le temps d'appel, il faut ajouter le paramètre `-t` lors de l'exécution : 

```bash
./drawmycall.py -f ./pcap_samples/sip-rtp-opus.pcap -t
```

Qui devrait produire quelque chose comme ceci : 

![13a2b0b48421b3bba3cccc2fd5ce62ac.png](images/a7ba3ef2956e4e889e3e5f25fb2c6f1a.png#center)

Enjoy :wink:

