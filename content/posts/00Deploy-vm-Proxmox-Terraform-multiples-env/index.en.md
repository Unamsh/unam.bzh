---
title: "ProxySQL failover with Keepalived"
author: "Unam"
date: "2022-10-02"
description: "How to configure keepalived to build a highly available ProxySQL cluster"
tags: ["mysql", "proxysql", "systemd"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
draft: true
series: ["ProxySQL"]
ShowBreadCrumbs: true
---

# Introduction
https://blog.gruntwork.io/how-to-manage-multiple-environments-with-terraform-using-workspaces-98680d89a03e


Enjoy :wink:
