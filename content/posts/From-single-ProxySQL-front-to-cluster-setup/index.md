---
title: "Passer d'un ProxySQL standalone à une implémentation en cluster"
author: "Unam"
date: "2023-01-12"
description: "Comment configurer ProxySQL en cluster"
tags: ["mysql", "proxysql", "systemd"]
categories: ["sysadmin"]
ShowToc: false
weight: 1
draft: false
series: ["ProxySQL"]
ShowBreadCrumbs: true
---

Dans [cet article](https://unam.bzh/fr/posts/mysql-proxy-proxysql-debian/) je parle du déploiement d'un serveur ProxySQL devant plusieurs serveurs MySQL.

Un ProxySQL c'est bien, mais deux ou plus c'est mieux. On parle alors d'un cluster. Ca peut être très utile (mais pas obligatoire !) pour préparer la prochaine étape : la haute disponibilité.

# Comment configurer ProxySQL

Depuis la version 2, ProxySQL dispose d'une fonction de réplication. Cela veut dire que ProxySQL est capable de répliquer plusieurs de ses tables de configuration vers un ou plusieurs hôtes.

> A noter qu'il n'y a pas de notions de master / slave, lorsqu'une configuration est modifiée sur l'un des membres du cluster, elle est propagée sur tous les membres.

Pour commencer, il est nécessaire d'avoir un second noeud ProxySQL.

Sur chacun des noeuds du cluster, dans le shell ProxySQL, il faut ajouter un nouvel utilisateur administrateur dans la variable `admin-admin_credentials` : 

```sql
update global_variables set variable_value='admin:admin;proxysql-cluster-admin:proxysql-cluster-admin-pass' where variable_name='admin-admin_credentials';
```

> Note : ici `admin:admin` est notre utilisateur administrateur initial. La variable peut contenir plusieurs comptes.

- On ajoute le compte `proxysql-cluster-admin` en tant qu'administrateur du cluster :  

```sql
update global_variables set variable_value='proxysql-cluster-admin' where variable_name='admin-cluster_username';
update global_variables set variable_value='proxysql-cluster-admin-pass' where variable_name='admin-cluster_password';
```

- On modifie la fréquence de vérification des changements 

```sql
update global_variables set variable_value=200 where variable_name='admin-cluster_check_interval_ms';
update global_variables set variable_value=100 where variable_name='admin-cluster_check_status_frequency';
```

- On ajoute les paramètres à synchroniser entre les membres du cluster

```sql
update global_variables set variable_value='true' where variable_name='admin-cluster_mysql_query_rules_save_to_disk';
update global_variables set variable_value='true' where variable_name='admin-cluster_mysql_servers_save_to_disk';
update global_variables set variable_value='true' where variable_name='admin-cluster_mysql_users_save_to_disk';
update global_variables set variable_value='true' where variable_name='admin-cluster_proxysql_servers_save_to_disk';
update global_variables set variable_value=3 where variable_name='admin-cluster_mysql_query_rules_diffs_before_sync';
update global_variables set variable_value=3 where variable_name='admin-cluster_mysql_servers_diffs_before_sync';
update global_variables set variable_value=3 where variable_name='admin-cluster_mysql_users_diffs_before_sync';
update global_variables set variable_value=3 where variable_name='admin-cluster_proxysql_servers_diffs_before_sync';
```

- A la fin, on charge les changements et on les écrit

```sql
load admin variables to runtime;
save admin variables to disk;
```

- Une dernière chose : on déclare les deux noeuds (ou plus) ProxySQL

```sql
INSERT INTO proxysql_servers (hostname,port,weight,comment) VALUES ('192.168.56.1',6032,100,'psql01');
INSERT INTO proxysql_servers (hostname,port,weight,comment) VALUES ('192.168.56.2',6032,100,'psql02');
LOAD PROXYSQL SERVERS TO RUNTIME;
SAVE PROXYSQL SERVERS TO DISK;
```

Dorénavant les tables `mysql_servers`, `mysql_query_rules`, `mysql_users`, `mysql_proxy_servers` seront répliquées à la moindre modification.

Comme indiqué au début, il n'y a pas de priorités sur les éditions : il est possible d'effectuer des changements sur n'importe quel membre du cluster.

Pour tester, on ajoute un utilisateur sur un des noeuds : 

```sql
ProxySQL>INSERT INTO mysql_users (username,password) values ('test','test');
```

Sur un autre noeud, on peut vérifier si l'utilisateur `test` existe 

```sql
ProxySQL> SELECT * from mysql_users;
```

Cette requête doit retourner les informations liées à l'utilisateur test. Si ce n'est pas le cas, vous pouvez consulter les logs de ProxySQL ici `/var/lib/proxysql/proxysql.log`.

Enjoy 😉