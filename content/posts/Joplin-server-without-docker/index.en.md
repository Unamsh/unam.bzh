---
title: "Install a Joplin synchro serveur without Docker"
author: "Unam"
date: "2021-11-15"
description : "How to deploy a Joplin synchronization server without docker"
tags: ["lxc", "joplin", "debian 11"]
categories: ["sysadmin", "system"]
ShowToc: false
weight: 2
ShowBreadCrumbs: true
---

Joplin is an Evernote alternative using markdown. The application can export your notes in multiples formats

![050f06a4be6147f0d4dccb39e254feff.png](images/ac.png#center)

Using the default setup, you can setup differents sync methods :

![28a33c3d7988a1ea70c457634e8d4f97.png](images/sync_joplin.png#center)

The simplest configuration is the "sync to file system" one. Using this, you can easily export all of your notes to a directory. With Nextcloud you can then sync with a remove server using webdav.

Nevertheless the synchronisation is very long and can be endless if you have many notes.

Joplin's core team has written a "native" synchronisation server who promises to sync very quickly and easily. And it's right.

The tools has a beautifull web interface for users, permissions etc.

Actually (at the end of 2021) the code comes by default with a docker container. As I do not use Docker on my server and I do not want to for the moment, here are some setup notes.

> tldr : If you want to try a quick setup, download  [this script](https://gist.githubusercontent.com/fulljackz/57d18869b1791bff2fe490c4a485393d/raw/7e00b50bde0be08c3ad57d34c15b330443addde8/Joplin-server-debian.md), change the vars and run it (HTH).

# Configuration

This configuration was tested on Debian 11 in a lxc container.

## Install Joplin server

- Change the vars to your needs (Only **changeme**)

```bash
export JOPLIN_HOME="/home/joplin"
export JOPLIN_URL="changeme" # WITHOUT HTTP OR HTTPS
export POSTGRES_ADM_PASS="changeme"
export POSTGRES_USER_PASS="changeme"
export POSTGRES_USER="joplindbuser"
export POSTGRES_DB="changeme"
```

- Install and create necessary files

```bash
apt update
apt -y install wget rsync sudo postgresql postgresql-client nginx rsyslog
mkdir /usr/local/src/joplin-server && cd $_
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/joplin-build.sh
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/joplin-requirements.sh
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/joplin.service
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/run.sh
chmod u+x ./joplin-requirements.sh
./joplin-requirements.sh
update-alternatives --install /usr/bin/python python /usr/bin/python2.7 2
update-alternatives --install /usr/bin/python python /usr/bin/python3.9 0
mv ./joplin-build.sh /home/joplin
mv ./run.sh /home/joplin
chown joplin: /home/joplin/*.sh
chmod u+x /home/joplin/*.sh
mv ./joplin.service /etc/systemd/system/joplin-server.service
cd /tmp/
```

- Setup postgres

```bash
su -c "psql -c \"ALTER USER postgres WITH password '${POSTGRES_ADM_PASS}'\"" postgres
su -c "createuser ${POSTGRES_USER}" postgres
su -c "createdb ${POSTGRES_DB} -O ${POSTGRES_USER}" postgres
su -c "psql -c \"ALTER USER ${POSTGRES_USER} WITH password '${POSTGRES_USER_PASS}';\"" postgres
su -c "${JOPLIN_HOME}/joplin-build.sh" joplin
sed -i "s|APP_BASE_URL.*|APP_BASE_URL=https:\/\/${JOPLIN_URL}|g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_PASSWORD.*/POSTGRES_PASSWORD=${POSTGRES_USER_PASS}/g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_DATABASE.*/POSTGRES_DATABASE=${POSTGRES_DB}/g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_USER.*/POSTGRES_USER=${POSTGRES_USER}/g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_HOST.*/POSTGRES_HOST=localhost/g" ${JOPLIN_HOME}/run.sh
```

- Reload services and check if Joplin is running

```bash
systemctl daemon-reload
systemctl restart joplin-server
curl localhost:22300/api/ping
```

### Nginx

- Generate a self signed cert

```bash
openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 \
    -subj "/C=FR/ST=Off/L=Off/O=Dis/CN=${JOPLIN_URL}" \
    -keyout /etc/ssl/private/${JOPLIN_URL}.key -out /etc/ssl/certs/${JOPLIN_URL}.crt
```

- Apply config

```bash
systemctl stop nginx
rm /etc/nginx/sites-enabled/default
echo "server {
    listen 443 ssl http2;
    server_name "${JOPLIN_URL}";

    proxy_read_timeout 720s;
    proxy_connect_timeout 720s;
    proxy_send_timeout 720s;

    client_max_body_size 50m;

    # Proxy headers
    proxy_set_header X-Forwarded-Host \$host;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Real-IP \$remote_addr;

    # SSL parameters
    ssl_certificate /etc/ssl/certs/${JOPLIN_URL}.crt;
    ssl_certificate_key /etc/ssl/private/${JOPLIN_URL}.key;
    ssl_protocols  TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers on;
    ssl_ciphers '"EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA HIGH !RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS"';

    # log files
    access_log /var/log/nginx/joplin.access.log;
    error_log /var/log/nginx/joplin.error.log;

    # Handle / requests and redirect to a specific port on localhost
    location / {
       proxy_redirect off;
       proxy_pass http://127.0.0.1:22300;
    }
}" >> /etc/nginx/sites-enabled/joplin
systemctl start nginx
```

- The web interface should be accessible using the ip or url of your container in a web browser. Enjoy.
