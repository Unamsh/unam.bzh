---
title: "Installer un serveur de synchronisation Joplin sans docker"
author: "Unam"
date: "2021-11-15"
description: "Comment déployer un serveur de synchro pour Joplin sans utiliser Docker"
tags: ["lxc", "joplin", "debian 11"]
categories: ["sysadmin", "system"]
ShowToc: false
weight: 1
ShowBreadCrumbs: true
---

Joplin est une alternative à Evernote. L'application supporte nativement le markdown et permet d'experter les notes vers plusieurs formats.

![050f06a4be6147f0d4dccb39e254feff.png](images/ac.png#center)

Avec la configuration par défaut il est possible d'utiliser différentes méthodes de synchronisation

![28a33c3d7988a1ea70c457634e8d4f97.png](images/sync_joplin.png#center)

La synchronisation vers le système de fichier est la configuration la plus simple et permet d'exporter toutes les notes vers un dossier spécifique. 
A l'aide de Nextcloud il est possible de synchroniser à l'aide du protocole webdav.

La synchronisation est toutefois très longue et peut être très longue lorsque plusieurs notes sont à envoyer et/ou recevoir.

Le développeur principal de Joplin s'est lancé dans l'écriture d'un serveur de synchronisation "natif", afin de se passer de webdav et ses lacunes.

L'outil dispose d'une belle interface web pour gérer les utilisateurs, leurs permissions etc.

Actuellement le code qui fait tourner le serveur est packagé dans un conteneur Docker. N'était pas utilisateur de Docker, j'ai pris quelques notes suite à l'installation dans un conteneur lxc.

> tldr : Pour installer Joplin serveur sans Docker, téléchargez  [ce script](https://gist.githubusercontent.com/fulljackz/57d18869b1791bff2fe490c4a485393d/raw/7e00b50bde0be08c3ad57d34c15b330443addde8/Joplin-server-debian.md), ajustez les variables et lancez le.

# Configuration

La configuration a été testée sur Debian 11 dans un conteneur lxc.

## Install Joplin server

- Editer les variables selon les besoins (Seulement **changeme**)

```bash
export JOPLIN_HOME="/home/joplin"
export JOPLIN_URL="changeme" # WITHOUT HTTP OR HTTPS
export POSTGRES_ADM_PASS="changeme"
export POSTGRES_USER_PASS="changeme"
export POSTGRES_USER="joplindbuser"
export POSTGRES_DB="changeme"
```

- Installer et créer les fichiers et dossiers nécessaires

```bash
apt update
apt -y install wget rsync sudo postgresql postgresql-client nginx rsyslog
mkdir /usr/local/src/joplin-server && cd $_
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/joplin-build.sh
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/joplin-requirements.sh
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/joplin.service
wget https://gist.githubusercontent.com/hi-ko/fbbd6f0f82955f55bb23c6f4db29bdb2/raw/73c69cbeb8d809952ca5cd38d8ca14203b90305f/run.sh
chmod u+x ./joplin-requirements.sh
./joplin-requirements.sh
update-alternatives --install /usr/bin/python python /usr/bin/python2.7 2
update-alternatives --install /usr/bin/python python /usr/bin/python3.9 0
mv ./joplin-build.sh /home/joplin
mv ./run.sh /home/joplin
chown joplin: /home/joplin/*.sh
chmod u+x /home/joplin/*.sh
mv ./joplin.service /etc/systemd/system/joplin-server.service
cd /tmp/
```

- Configuration de la base de donnée postgres

```bash
su -c "psql -c \"ALTER USER postgres WITH password '${POSTGRES_ADM_PASS}'\"" postgres
su -c "createuser ${POSTGRES_USER}" postgres
su -c "createdb ${POSTGRES_DB} -O ${POSTGRES_USER}" postgres
su -c "psql -c \"ALTER USER ${POSTGRES_USER} WITH password '${POSTGRES_USER_PASS}';\"" postgres
su -c "${JOPLIN_HOME}/joplin-build.sh" joplin
sed -i "s|APP_BASE_URL.*|APP_BASE_URL=https:\/\/${JOPLIN_URL}|g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_PASSWORD.*/POSTGRES_PASSWORD=${POSTGRES_USER_PASS}/g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_DATABASE.*/POSTGRES_DATABASE=${POSTGRES_DB}/g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_USER.*/POSTGRES_USER=${POSTGRES_USER}/g" ${JOPLIN_HOME}/run.sh
sed -i "s/POSTGRES_HOST.*/POSTGRES_HOST=localhost/g" ${JOPLIN_HOME}/run.sh
```

- Recharger les services et vérifiez que Joplin-server fonctionne

```bash
systemctl daemon-reload
systemctl restart joplin-server
curl localhost:22300/api/ping
```

### Nginx

- Générer un certificat auto signé

```bash
openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 \
    -subj "/C=FR/ST=Off/L=Off/O=Dis/CN=${JOPLIN_URL}" \
    -keyout /etc/ssl/private/${JOPLIN_URL}.key -out /etc/ssl/certs/${JOPLIN_URL}.crt
```

- Appliquer la configuration 

```bash
systemctl stop nginx
rm /etc/nginx/sites-enabled/default
echo "server {
    listen 443 ssl http2;
    server_name "${JOPLIN_URL}";

    proxy_read_timeout 720s;
    proxy_connect_timeout 720s;
    proxy_send_timeout 720s;

    client_max_body_size 50m;

    # Proxy headers
    proxy_set_header X-Forwarded-Host \$host;
    proxy_set_header Host \$host;
    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto \$scheme;
    proxy_set_header X-Real-IP \$remote_addr;

    # SSL parameters
    ssl_certificate /etc/ssl/certs/${JOPLIN_URL}.crt;
    ssl_certificate_key /etc/ssl/private/${JOPLIN_URL}.key;
    ssl_protocols  TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers on;
    ssl_ciphers '"EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA HIGH !RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS"';

    # log files
    access_log /var/log/nginx/joplin.access.log;
    error_log /var/log/nginx/joplin.error.log;

    # Handle / requests and redirect to a specific port on localhost
    location / {
       proxy_redirect off;
       proxy_pass http://127.0.0.1:22300;
    }
}" >> /etc/nginx/sites-enabled/joplin
systemctl start nginx
```

  - L'interface web d'administration doit être accessible à l'url ou l'adresse ip du serveur nginx.
