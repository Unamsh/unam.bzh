---
title: "Start playing with a Yubikey on Ubuntu 22.10"
author: "Unam"
date: "2023-03-27"
description: "Few notes to remember how to configure sudo, open a session or unlock keepass using a Yubikey 5C NFC"
tags: ["ubuntu", "yubikey", "security"]
#categories: [""]
ShowToc: false
weight: 2
draft: true
#series: [""]
ShowBreadCrumbs: true
---

