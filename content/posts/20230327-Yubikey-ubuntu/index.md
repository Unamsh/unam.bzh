---
title: "A la découverte de la Yubikey 5c NFC sous Ubuntu 22.10"
author: "Unam"
date: "2023-03-27"
description: "Quelques notes pour se souvenir comment paramétrer quelques applications comme sudo ou encore déverouiller une session ou kepassxc à l'aide d'une Yubikey 5C nFC sous Ubuntu 22.10"
tags: ["ubuntu", "yubikey", "security"]
#categories: [""]
ShowToc: false
weight: 1
draft: true
#series: [""]
ShowBreadCrumbs: true
---