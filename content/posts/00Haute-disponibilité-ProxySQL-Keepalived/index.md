---
title: "ProxySQL failover avec Keepalived"
author: "Unam"
date: "2022-10-02"
description: "Comment mettre en place de la haute disponibilité pour ProxySQL avec Keepalived"
tags: ["mysql", "proxysql", "systemd"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
draft: true
series: ["ProxySQL"]
ShowBreadCrumbs: true
---

# Introduction



Enjoy :wink:
