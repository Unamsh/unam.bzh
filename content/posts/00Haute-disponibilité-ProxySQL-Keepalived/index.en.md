---
title: "ProxySQL HA with Keepalived"
author: "Unam"
date: "2022-10-02"
description: "How to configure high availability for ProxySQL with Keepalived"
tags: ["mysql", "proxysql", "systemd"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
draft: true
series: ["ProxySQL"]
ShowBreadCrumbs: true
---

# Introduction



Enjoy :wink:
