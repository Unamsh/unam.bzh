---
author: "Unam"
title: "Lxd to Proxmox lxc migration"
date: "2022-01-01"
description: "How to migrate lxd containers to lxc on Proxmox 7"
tags: ["lxd", "lxc", "proxmox", "debian"]
categories: ["proxmox", "sysadmin"]
ShowToc: false
weight: 2
ShowBreadCrumbs: false
---

I used a [Scaleway Dedibox Start-2-S-SATA](https://www.scaleway.com/fr/dedibox/start/start-2-s-sata/) server for few years.
Low price and important storage were the two arguments for choosing it. 
With time, new services were installed on it using lxd containers for all of them on Debian 11.

Lxd daemon is quite cool and allows anyone without important lxc knowledge to deploy and install new services using the cli.

The latest deployement I made was a synapse server [for matrix](https://matrix.org) coming with a (one more) Postgres database. It was the non return point : global perf were so bad, all my web interfaces were slow and the important resource comsuption of one of the services impacted all the others.

For information, until this day I was able to host this services (1/ct) on this little Atom :
- mails 
- reverse proxy
- notes
- a web server
- a Matrix instance
- a Nextcloud instance

> All of them are services I daily use.

After a quick search I found a new server at Hetnzer in the [auction category](https://www.hetzner.com/sb) with a Xeon(4c/8th), 32Gb of ram and 2x2TB HDD versus the Scaleway Atom dual core, 4B ram and single 1TB hdd.

***Pros :***
- Huge perf improvement
- Proxmox ready !
- "Security" with raid1
- Ipv6 ready (not configured yet :grimacing:)
- Hosted in Germany

***Cons :***
- Twice the price
- No ssd but it makes the job

## Lxd export and lxc import

After a simple deboostrap setup (that maybe I'll share here one day), I made a Proxmox setup with a thin provisionning logical volume for my vms and containers.

There is no "easy" way to import lxd managed containers into Proxmox. Here are the steps I tried for an "automagic" import :

- On source : `lxc export` to a tarball
- Copy this tarball from source and target using `scp`
- Create a new lxc container on the target with the Proxmox web ui
- Mount the new filesystem into a directory with `pct mount` and delete all its content with `rm`
- Extract the tarball and rsync its content to the directoy used above

Ok, now everything is ok and the container is starting but I have to deal with ownership errors and uid / gid wrong mappings.
After some research and tests, I decided to tried something else. Reading [this gist](https://gist.github.com/mbuiro/3b1d79d75ab17c0e7cb24cff6767b636) gave me the tip !

## Good old tar and Proxmox

Simple problem, simple 3 lines solution for export and 1 line for import on target :ok_hand:

```bash
lxc exec haproxyct -- bash -c "cd / && tar --exclude=run --exclude=dev --exclude=sys --exclude=proc -czf /haproxy.tar.gz ./*"
lxc file pull "haproxyct/haproxyct.tar.gz" /home/
scp /home/haproxyct.tar.gz user@furious-new-dedicated-server:/
```

And on target using `pct` tool I just have to 

```bash
pct create 100 /haproxyct.tar.gz -hostname haproxyct --rootfs lv_thin:10 --net0 name=eth0,ip=10.10.10.10/24,bridge=vmbr0,gw=10.10.10.254
```

> Do not forget to change `--exclude` if you want to add more and the args used with `pct`

That's it, the job is done and everything is working !

Enjoy :wink:
