---
author: "Unam"
title: "Migrer de lxd vers lxc sous Proxmox"
date: "2022-01-01"
description: "Comment migrer depuis des conteneurs lxd lxc sous Proxmox 7"
tags: ["lxd", "lxc", "proxmox", "debian"]
categories: ["proxmox", "sysadmin"]
ShowToc: false
weight: 1
draft: false
ShowBreadCrumbs: false
---

J'ai utilisé un serveur [Dedibox Start-2-S-SATA](https://www.scaleway.com/fr/dedibox/start/start-2-s-sata/) pendant plusieurs années.

Choix effectué par rapport au prix et au stockage disponible. Avec le temps j'ai déployé de plus en plus de services dessus dans des conteneurs lxd.

Lxd est un super outil, il permet à n'importe qui ayant assez peu de connaissances sous lxc de déployer et installer des nouveaux services en ligne de commande.

La dernière installation de services avant la migration fut l'installation d'un serveur [synapse pour matrix](https://matrix.org), accompagné d'une base de données Postgres. Ce fut le point de non retour, les perfs étaient catastrophiques. Tous les services que j'utilisais étaient impactés, interfaces lentes, stockage lent etc.

Pour rappel, avant ce jour voici la totalié des services qui tournaient sur ce petit Atom :
- mails 
- reverse proxy
- notes
- serveur web
- instance Matrix
- instance Nextcloud

> Tous ces services servent quotidiennement

Je me suis donc mis en quête d'un nouveau serveur dédié et suis tombé sur une offre sympa chez Hetzner [dans la catégorie auction](https://www.hetzner.com/sb). La configuration est composée de 
- Xeon(4c/8th)
- 32Go de ram
- 2x2TB HDD 


***Pour :***
- Gain de performances général
- Proxmox ready !
- "Sécurité" avec la possibilité de faire un raid1
- Ipv6 ready (Pas encore configuré :grimacing:)
- Hébergé en Allemagne

***Contre :***
- Le double du prix originel
- Pas de SSD

## Lxd export et lxc import

Après un simple deboostrap, j'ai installé Proxmox et configuré un volume en thin provisionning pour les vms et les conteneurs.

A problème simple, réponse simple. En 3 lignes il est possible d'exporter le contenu d'un conteneur lxd dans une archive, puis de pousser cette archive sur un hôte distant : 

```bash
lxc exec haproxyct -- bash -c "cd / && tar --exclude=run --exclude=dev --exclude=sys --exclude=proc -czf /haproxy.tar.gz ./*"
lxc file pull "haproxyct/haproxyct.tar.gz" /home/
scp /home/haproxyct.tar.gz user@furious-new-dedicated-server:/
```

> Il est possible d'ajouter autant d'`--exclude`que souhaité lors de l'export.

Puis sur la cible créer un conteneur en utilisant la commande `pct` et l'archive importée précédemment : 

```bash
pct create 100 /haproxyct.tar.gz -hostname haproxyct --rootfs lv_thin:10 --net0 name=eth0,ip=10.10.10.10/24,bridge=vmbr0,gw=10.10.10.254
```

And voila. Le conteneur est importé et fonctionnel.

Enjoy :wink:
