---
title: "Haute disponibilité pour ProxySQL avec Keepalived"
author: "Unam"
date: "2023-01-19"
description: "Comment mettre en place de la haute disponibilité pour ProxySQL à l'aide de Keepalived"
tags: ["mysql", "proxysql", "systemd", "proxysql"]
categories: ["sysadmin"]
ShowToc: true
weight: 1
draft: false
series: ["ProxySQL"]
ShowBreadCrumbs: true
---

Lorsque l'on dispose d'un cluster de ProxySQL, il peut être intéressant d'activer un mécanisme de haute disponibilité afin de basculer sur un autre noeud en cas de perte du serveur initial.

Keepalived se prête assez bien au jeu de l'adresse ip virtuelle et dispose de mécanismes pour s'assurer de la bascule de la vip entre les différents noeuds d'un "cluster".

Sous Debian rien de plus simple, il suffit d'installer depuis les dépôts : 

```
apt update 
apt install keepalived
```

La configuration ci-dessous reprend tous les besoins : 

- Configuration d'une adresse ip virtuelle
- Bascule automatique en cas de "perte" du processus proxysql
- Maintient en secondaire même si le processus ProxySQL apparait de nouveau

```
global_defs {
        notification_email {
            user@ndd.tld
        }
        notification_email_from srv@ndd.tld
        smtp_server my.mail.relay.server
        smtp_connect_timeout 30
        enable_script_security
        script_user root
}

vrrp_script chk_proxysql {
   script "/usr/bin/killall -0 proxysql"
   interval 1
   fall 2
   rise 2
}
vrrp_instance VI_PROXYSQL {
   interface ens192
   state BACKUP
   virtual_router_id 51
   smtp_alert
   nopreempt
   priority 101
   unicast_src_ip 192.168.56.1
   unicast_peer {
      192.168.56.2

   }
   virtual_ipaddress {
       192.168.56.100
   }
   track_script {
       chk_proxysql
   }
}
```

Copier / coller la configuration ci-dessous sur l'ensemble des noeuds du cluster dans `/etc/keepalived/keepalived.conf`.
Il est ensuite nécessaire de modifier les paramètres ci-dessous : 

- notification_email : destination des emails de notification
- notification_email_from : source des emails de notification
- smtp_server : le serveur mail par lequel émettre les emails de notification
- unicast_src_ip : l'adresse ip du noeud sur lequel le fichier de configuration est collé
- unicast_peer : l'adresse du second noeud membre du cluster
- virtual_ipaddress : l'adresse ip virtuelle du cluster

Il reste à redémarrer keepalived : 

```
systemctl restart keepalived
```

Enfin, appliquer la même configuration sur le second noeud ProxySQL (en tâchant d'inverser `unicast_src_ip` et  `unicast_peer` 😉)

Pour tester, il est possible de "killer sauvagement" keepalived afin de vérifier que la vip bascule bien sur le second noeud.

Sur le noeud tenant la vip, taper la commande suivante : 

```
for x in $(ps -ef | grep proxysql | awk '{print $2}'); do kill -9 $x; done
```

La vip a du basculer sur l'autre serveur ProxySQL, enjoy :wink:
