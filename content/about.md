---
#author: "Emmanuel Le Nohaïc"
title: "About"
#description: "Emmanuel Le Nohaïc's about webpage : another sysadmin description :/"
ShowReadingTime: false
ShowToc: false
weight: 2
params:
ShowShareButtons: true
---

This is a simple "About" page written with vim and Tilix terminal (nobody cares !).

Few things about me : I am a network and system administrator mainly interested by free and open source softwares. I also like to build networks and troubleshoot complex IT problems / incidents.

Here are some tools and softwares I daily use :
- Debian, Ubuntu, centos (sometimes, when I get depressed)
- Bash / Python scripting
- Firewalling with iptables / firewalld
- Automation with Gitlab-ci and Jenkins
- Templates and deployements using packer / terraform on vmware
- Administration of multiples Proxmox VE and Proxmox Backup server

This list is non exhaustive and has to be completed. 
If you need more informations (or if you just want to talk :smiley:), do not hesitate to contact me using informations on the landing page.

## Footer

I try (most of the time I spend here) to write in english. This permits me to improve my technical skills and reflexions speed. I also try to traduce from english to french but as I do it very quick you will maybe find mistakes or typo due.

Please don't hesitate to contact me if it hurts your eyes ! :grinning: